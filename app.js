// DOM MANIPULATIONS

// debugger - it's stops/pause executing code.

let i = 0;
function change() {
  if (i < 1) {
    var switcher = document.body;
    switcher.classList.toggle('mode');
    let text = document.createElement('LI');
    text.textContent = "You switched page to a dark mode";
    switcher.appendChild(text);
    let div1 = document.querySelector("#div1");
    div1.after(text);
  } else if (i < 2) {
    var switcher2 = document.body;
    switcher2.classList.toggle('mode');
    let text2 = document.createElement('LI');
    text2.innerHTML = "<b>You switched page to a light mode back again and it's over :)</b>";
    switcher2.appendChild(text2);
    let div1 = document.querySelector("#div1");
    div1.after(text2);
  } else
    return;
  i++
}


// change CSS 

function change2() {
  console.log("change button color using queryselector ")
  let button2 = document.querySelector('#div2');
  button2.style.backgroundColor = "violet";
}

// change classes 

function change3() {
  console.log("changed class")
  let button3 = document.querySelector('.div3');
  button3.classList.toggle('newDiv3')
}

// add class

function change4() {
  console.log("class added");
  let button4 = document.querySelector('.div4');
  button4.classList.add('newDiv4');
}

// add class eventListener

let button5 = document.querySelector('.div5')

button5.addEventListener("click", () => {
  button5.classList.add('newDiv4')
  console.log("class added using eventListener");
})

// querySelectAll
let button6 = document.querySelector('.div6');
var names = document.querySelectorAll('.names li');

button6.addEventListener("click", () => {
  console.log(names)

  let surname = document.createElement('p');
  surname.innerHTML = "<b>Jerry can be name of a cat or name of a man</b>";
  names[1].append(surname);
  console.log(typeof (surname));

});

//  standard function
for (n of names) {
  n.addEventListener("click", function () {
    console.log(this);
    this.style.color = "blue";
    console.log("standard function is gonna be blue")
  }
  )
}

// arrow function 

for (m of names) {
  m.addEventListener("click", (a) => {
    console.log(a.target);
    console.log("arrow function");
  })
}

let input = document.querySelector('.list');
let addList = document.querySelector('.addList');
let nameList = document.querySelector('.names');

addList.addEventListener("click", function () {
  // create an list
  const newList = document.createElement('LI');
  // add the input value inside that input li
  let liContent = document.createTextNode(input.value);
  console.log(`${input.value} is a name you added `);
  newList.appendChild(liContent);
  //attach li to the names
  nameList.appendChild(newList);
});


function change5() {
  let style = `
  background-color: violet;
  color: white;
  border: 1px solid black;
  font-size: 35px`
  console.log("%c diffrent style is displayed", style);
  console.log("%c another style is displayed", "background-color: blue; font-size: 23px; letter-spacing: 1rem")

  // %c for a CSS

  //  %s for a String

  // %d or %i for Number

  // %f for Floating points

  // %o for an Object

  // %j for an JSON
}

// setInterval function scheme setInterval(function () {element.innerHTML += "Hello"}, 1000);
function time() {
  let time = document.querySelector('#time');
  let currentTime = new Date();
  setInterval(function(){
  time.innerHTML = new Date().toLocaleString();
  },1000);
  let style2 = `
  background-color: violet;
  color: white;
  border: 1px solid black;
  font-size: 35px`
  console.log("%cThis is the current time", style2, currentTime);
}

function jq(){
  $("span.jq").text("test if it works!")
  console.log("jq works!")
};



function styl(){
  let styless = `
  background-color: violet;
  color: white;
  border: 1px solid black;
  font-size: 35px`
  
  $("div#styles").animate({
    width: '100px',
    height: '100px',
    padding: '10px',
    border: 'none',
    color: 'white'
  }, 1000);
  $("p#styles2").show().animate(styless, 2000);
};